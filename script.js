// JavaScript source code

var count;
var d = new Date();

$('.editStyleFormBlock').hide();

function checkTime(i)
{
    if (i < 10)
    {
        i = "0" + i;
    }
    return i;
}
var date = checkTime(d.getDate()) + "." + checkTime(d.getMonth() + 1) + "." + d.getFullYear();

var time = checkTime(d.getHours()) + "." + checkTime(d.getMinutes()) + "." + checkTime(d.getSeconds());

function editEditableText()
{
    var textHtmlEditable = document.getElementById('text').value;
    var textSizeEditable = document.getElementById('size').value;

    if (textHtmlEditable != '' || textSizeEditable != '')
    {
        $('div.template').css({
            'font-size': (textSizeEditable + 'px'),
            'font-family': textHtmlEditable
        });

        saveTemplate();
    }
    else
    {
        alert('Заполните правильно колонки!');
    }
}

function saveTemplate()
{
    var content     = $('#placeholder').html();
    var urlParams   = new URLSearchParams(window.location.search);
    var templateId  = urlParams.get('templateId');
    var data        = getUserData();

    $.each(data, function(index, value) {
        if (value.id == templateId) {
            data[index].template = content;
            data[index].modified = $.now();
        }
    });

    //@todo emulate sending data to server

    localStorage.setItem('data', JSON.stringify(data));
}

function getUserData() {
    if (!localStorage.data) {
        try {
            $.getJSON('data.json', function (data)
            {
                console.log(data.users);
                localStorage.setItem('data', JSON.stringify(data.users));
            });
        } catch (ex) {
            console.log(ex);
        }
    }

    return JSON.parse(localStorage.getItem('data'));
}

function Backwards()
{
    document.location.href = 'page.html';
}

function bigImg(x)
{
    $('.editStyleFormBlock').show();
}

function normalImg(x)
{

}